<?php

namespace FreePBX\modules;

require 'BMore.php';

class Moe extends \FreePBX\Builtin\BMore implements \BMO {
	public function __construct($freepbx = null) {
		parent::__construct($freepbx);

		$this->schema = array(
			'apps' => array(
				'table' => 'moe_apps',
				'fields' => array(
					'id' => array(
						'sqltype' => 'INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT',
						'key' => True,
						'header' => 'id',
					),
					'extension' => array(
						'sqltype' => 'VARCHAR(64)',
						'header' => _("Extension"),
						'help' => _("Sequence to dial that activates application"),
					),
					'application' => array(
						'sqltype' => 'VARCHAR(256)',
						'header' => _("Application"),
						'help' => _("Asterisk application or expression"),
					),
					'answer' => array(
						'sqltype' => 'TINYINT(1)',
						'header' => _("Answer"),
						'help' => _("Answer the call before executing app"),
						'default' => true,
						'data-formatter' => <<<'JS'
return '<i class="fa fa-' + ((value == 0) ? '' : 'check-') + 'square-o"></i>';
JS
					),
				),
				'views' => array(
					'list' => array(
						'table' => array(
							'toolbar' => array(
								'btn|[plus] New App' => '#edit',
								'btn|[info-circle] Asterisk Apps' => '@astapps/list',
							),
							'fields' => array(
								'extension',
								'answer',
								'application',
								'action' => array(
									'header' => _("Action"),
									'data-formatter' => <<<'JS'
var edit = '<a href="?display=$module_name&_t=apps&_v=edit&id='+row.id+'"><i class="fa fa-edit"></i></a>';
var del = '&nbsp;<a class="delAction" href="?display=$module_name&_t=apps&_a=delete&id='+row.id+'"><i class="fa fa-trash"></i></a>';
return edit + del;
JS
								),
							),
						),
					),
					'edit' => array(
						'menu' => array(
							'btn|[info-circle] Asterisk Apps' => '@astapps/list',
						),
						'html' => array('<p></p>'),
						'form' => array(
							'postview' => 'list',
							'fields' => array(
								'extension',
								'answer',
								'application',
							),
						),
						'buttons' => array(
							'submit' => _("Submit"),
							'cancel' => _("Cancel"),
						),
					),
				),
			),
			'astapps' => array(
				'fields' => array(
					'name' => array(
						'header' => _("Name"),
					),
					'description' => array(
						'header' => _("Description"),
					),
				),
				'views' => array(
					'list' => array(
						'table' => array(
							'params' => array(),
						),
					),
				),
			),
			'default' => array(
				'views' => array(
					'page' => array(
						'html' => array(
							'div class="container-fluid"' => array(
								'h1' => 'Moe Apps <span id="cluster_header_domain"></span>',
								'div class="fpbx-container"' => array(
									'div class="display full-border"' => '$content'
								)
							)
						)
					),
					'rnav' => array(
						'table' => array(
							'params' => array(
								'class' => array('table'),
								'data-cache' => 'false',
								'data-toggle' => 'table',
								'data-seaarch' => 'true',
							)
						)
					)
				)
			),
		);
	}

	public function myDialplanHooks() {
		return true;
	}

	public function doDialplanHook($ext, $engine, $priority) {
		$id = 'from-internal-moe';
		$ext->addInclude('from-internal-additional', $id);

		foreach ($this->getAllRecords($this->getContext(Null, 'apps')) as $app) {
			if ($app['answer']) {
				$ext->add($id, $app['extension'], '', new \ext_answer());
			}
			$ext->add($id, $app['extension'], '', new \extension($app['application']));
		}
	}

	public function addRecord_apps($context, $record) {
		\needreload();
		return $this->addRecord($context, $record);
	}
	public function updateRecord_apps($context, $record) {
		\needreload();
		return $this->updateRecord($context, $record);
	}
	public function deleteRecord_apps($context, $record) {
		\needreload();
		return $this->deleteRecord($context, $record);
	}

	public function getAllRecords_astapps() {
		$records = array();
		foreach (explode("\n", shell_exec('asterisk -rx "core show applications"')) as $line) {
			$split = explode(':', $line);
			if (!empty($split[1])) {
				$records[] = array(
					'name' => trim($split[0]),
					'description' => trim($split[1]),
				);
			}
		}
		return $records;
	}
}
